# vue3-test

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
启动报错。。好像没有权限
查看权限
get-ExecutionPolicy
输出：
Restricted

受限制的，表示状态是禁止的，那就给个权限：
Set-ExecutionPolicy -Scope CurrentUser

位于命令管道位置 1 的 cmdlet Set-ExecutionPolicy
请为以下参数提供值:
ExecutionPolicy: RemoteSigned
> 成功启动


# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

以上内容是vue3做好脚手架后自己生成的内容



### 目录解析

| 目录/文件    | 说明                                                         |
| ------------ | ------------------------------------------------------------ |
| build        | 项目构建(webpack)相关代码                                    |
| config       | 配置目录，包括端口号等。我们初学可以使用默认的。             |
| node_modules | npm 加载的项目依赖模块                                       |
| src          | 这里是我们要开发的目录，基本上要做的事情都在这个目录里。里面包含了几个目录及文件：    assets: 放置一些图片，如logo等。  components: 目录里面放了一个组件文件，可以不用。  App.vue: 项目入口文件，我们也可以直接将组件写这里，而不使用 components 目录。  main.js: 项目的核心文件。 index.css: 样式文件。 |
| static       | 静态资源目录，如图片、字体等。                               |
| public       | 公共资源目录。                                               |
| test         | 初始测试目录，可删除                                         |
| .xxxx文件    | 这些是一些配置文件，包括语法配置，git配置等。                |
| index.html   | 首页入口文件，你可以添加一些 meta 信息或统计代码啥的。       |
| package.json | 项目配置文件。                                               |
| README.md    | 项目的说明文档，markdown 格式                                |
| dist         | 使用 npm run build 命令打包后会生成该目录。                  |

https://gitee.com/zy_laoyang/vue3-test.git

h5/hw.html独立运行还可以，但是不能做脚手架案例

demo1.html 自动增长的记数器。

vue3 生命周期：

- beforeCreate( ) ：在实例生成之前会自动执行的函数
- created( ) : 在实例生成之后会自动执行的函数
- beforeMount( ) : 在模板渲染完成之前执行的函数
- mounted( ) : 在模板渲染完成之后执行的函数
- beforeUpdate ：当data中的数据变化时， 会立即自动执行的函数
- updated：当data中的数据发生变化，页面重新渲染完后，会自动执行的函数
- beforeUnmount( ) :当Vue应用失效时，会自动执行的函数
- unmounted() : 当Vue应用失效时，且DOM完全销毁之后，会自动执行

在`template`属性中的`{{message}}`就是插值表达式

`v-html`的标签解决message中包含想起做用的标签问题。<h2 v-html="message"> </h2>

message 的值 在改变时，会影响引用的地方，所以如果不需要影响

那么可以引用的时候，使用 v-once 属性

`v-bind`标签 可以绑定相关属性值

 @click.prevent用于阻止默认行为

```vue
<form
    action="https://jspang.com"
    @click.prevent="hanldeButton">
    <button type="submit">默认提交</button>
</form>
```


改建另一个脚手架工具ｖｉｔｅ
